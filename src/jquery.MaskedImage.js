/**
 * MaskedImage jQuery Plugin
 * 
 * Released under the MIT license
 * 
 * @fileOverview    グレースケール画像をマスクとしたアルファブレンドを実装します。
 * @version         1.4
 * @requires        jQuery 1.6+
 * @copyright       Copyright (c) 2015 surface0
 * @link            https://gitlab.com/seizu/jquery.masked-image/ MaskedImage Project
 * @license         http://www.opensource.org/licenses/mit-license.php MIT License
 */

!function($){
    "use strict";

    var _canvasPreMultipliedAlpha = null;

    var _UseMode = {
        SVG:    1,
        IMAGE:  2,
        CANVAS: 3,
    };

    var _defaults = {
        useSvg: true,                   // priority svg > (img) > canvas
        useImage: true,                 // if useable  
        usePreMultipliedAlpha: null,    // null/true/false
        canvasScale: 1,                 // >= 1
    };

    $.fn.MaskedImage = function(config) {
        var options = _makeOptions(config);

        return this.each(function() {
            if (this.tagName.toLowerCase() === "img") {
                _replaceImageElement($(this), options);
            }
            return;
        });
    };

    // TODO: Image渡しの非同期版も実装予定
    $.MaskedImage = function(config) {
        var options = _makeOptions(config);
        return _createMaskedElement(config.url.base, config.url.mask, options);
    };

    /**
     * @param {Object} config
     * @return {Object}
     */
    function _makeOptions(config) {
        var options = $.extend({}, _defaults, config);
        
        if (options.useSvg && _checkEnableSVGMask()) {
            options._useMode = _UseMode.SVG;
        } else {
            options._useMode = options.useImage ? _UseMode.IMAGE : _UseMode.CANVAS;
        }
        
        return options;
    }

    /**
     * @param {Image} baseImg
     * @param {Image} maskImg
     * @param {Object} options
     * @return {jQueryDeferred}
     */
    function _createMaskedElement(baseUrl, maskUrl, options) {
        var createElementFunction = options._useMode === _UseMode.SVG
            ? _createMaskedSVG
            : _createMaskedCanvas;
            
        return _preloadImages(baseUrl, maskUrl)
            .pipe(function(baseImg, maskImg) {
                return createElementFunction(baseImg, maskImg, options);
            })
            .pipe(function(result) {
                if (options._useMode === _UseMode.IMAGE) {
                    var src = result.toDataURL();
                    // Android2.x系対応
                    if (src !== "data:,") {
                        var img = new Image();
                        img.src = src;
                        return img;
                    }
                }
                
                return result;
            });
    }

    /**
     * @param {jQueryObject} $target
     * @param {Object} options
     */
    function _replaceImageElement($target, options) {
        var sizeOptions = {
            width: $target.attr("width"),
            height: $target.attr("height"),
        };
        
        options = $.extend({}, sizeOptions, options);
        
        var baseUrl = $target.attr("src");
        var maskUrl = $target.attr("data-mask");
        
        if (!baseUrl || !maskUrl) {
            return;
        }
        
        _createMaskedElement(baseUrl, maskUrl, options)
            .done(function(elm) {
                $target.replaceWith(elm);
            });
    }

    /**
     * @param {String} baseUrl
     * @param {String} maskUrl
     * @return {jQuery.Deferred}
     */
    function _preloadImages(baseUrl, maskUrl) {
        var baseImg = new Image();
        var maskImg = new Image();

        var otherLoaded = false;
        var dfd = $.Deferred();
        var onLoad = function() {
            if (otherLoaded) {
                dfd.resolve(baseImg, maskImg);
            }
            otherLoaded = true;
        };
        
        baseImg.onload = onLoad;
        maskImg.onload = onLoad;
        baseImg.src = baseUrl;
        maskImg.src = maskUrl;
        
        return dfd.promise();
    }

    /**
     * @param {String} name
     * @param {Object} attrs
     * @param {Element}
     */
    function _createSVGElement(name, attrs) {
        attrs = attrs || {};
        
        var elem = document.createElementNS("http://www.w3.org/2000/svg", name);
        
        Object.keys(attrs).forEach(function(key) {
            var ns = key === "href" ? "http://www.w3.org/1999/xlink" : null;
            elem.setAttributeNS(ns, key, attrs[key]);
        });
        
        return elem;
    };

    /**
     * @param {jQueryObject} $target
     * @param {Image} baseImg
     * @param {Image} maskImg
     * @param {Object} options
     */
    function _createMaskedSVG(baseImg, maskImg, options) {     
        var w = baseImg.width;
        var h = baseImg.height;
        
        var svg = _createSVGElement("svg", {
            viewBox: "0 0 " + w + " " + h,
            width: options.width,
            height: options.height,
        });
        var defs = _createSVGElement("defs");
        var mask = _createSVGElement("mask", {
            x: 0,
            y: 0,
            width: w,
            height: h,
            maskUnits: "userSpaceOnUse",
            maskContentUnits: "userSpaceOnUse",
        });
        var maskImage = _createSVGElement("image", {
            width: w,
            height: h,
            href: maskImg.src,
        });
        var baseImage = _createSVGElement("image", {
            x: 0,
            y: 0,
            width: w,
            height: h,
            mask: "url(#mask)",
            href: baseImg.src,
        });
        
        mask.id = "mask";
        mask.appendChild(maskImage);
        defs.appendChild(mask);
        svg.appendChild(defs);
        svg.appendChild(baseImage);
        
        return svg;
    }
    
    /**
     * @param {Image} baseImage
     * @param {Image} maskImage
     * @param {Boolean} preMultipleAlpha
     * @return {HTMLCanvasElement}
     */
    function _createMaskedCanvas(baseImg, maskImg, options) {
        // Canvasの大きさをベース画像基準にデフォルト設定
        options = $.extend({}, {
            width: baseImg.width,
            height: baseImg.height,
        }, options);
        
        var usePreMultipliedAlpha = typeof options.usePreMultipliedAlpha === "boolean"
            ? options.usePreMultipliedAlpha
            : _checkCanvasPreMultipliedAlpha();

        var w = options.width * options.canvasScale;
        var h = options.height * options.canvasScale;
        var base = _createCanvas(baseImg, w, h);
        var mask = _createCanvas(maskImg, w, h);
        var baseData = base.imageData.data;
        var maskData = mask.imageData.data;
    
        if (usePreMultipliedAlpha) {
            var len = maskData.length, alpha;
            var preMA = function(sa, sv) {
                return (sv * (sa / 255) + 0.5) >> 0;
            };
            
            var i = 0;
            for (var offset = 0; offset < len; offset = (offset + 4)|0) {
                i = offset|0;
                alpha = maskData[offset];
                baseData[i] = alpha && preMA(alpha, baseData[i]); i = (i+1)|0;
                baseData[i] = alpha && preMA(alpha, baseData[i]); i = (i+1)|0;
                baseData[i] = alpha && preMA(alpha, baseData[i]); i = (i+1)|0;
                baseData[i] = alpha;
            }
        } else {
            // Duff's device
            var iteration = (Math.floor(maskData.length / 4 / 8) + 1)|0,
                startAt = maskData.length % 8,
                i = 0;

            do {
                switch (startAt) {
                    case 0: baseData[(i+3)|0] = maskData[i]; i = (i+4)|0;
                    case 7: baseData[(i+3)|0] = maskData[i]; i = (i+4)|0;
                    case 6: baseData[(i+3)|0] = maskData[i]; i = (i+4)|0;
                    case 5: baseData[(i+3)|0] = maskData[i]; i = (i+4)|0;
                    case 4: baseData[(i+3)|0] = maskData[i]; i = (i+4)|0;
                    case 3: baseData[(i+3)|0] = maskData[i]; i = (i+4)|0;
                    case 2: baseData[(i+3)|0] = maskData[i]; i = (i+4)|0;
                    case 1: baseData[(i+3)|0] = maskData[i]; i = (i+4)|0;
                }
                startAt = 0;
                
                iteration = (iteration - 1)|0;
            } while (iteration);
        }

        base.context.putImageData(base.imageData, 0, 0);
        
        return base.canvas;
    }
    
    /**
     * @return {Boolean}
     */
    function _checkEnableSVGMask() {
        return window.SVGMaskElement && window.SVGFilterElement;
    }
    
    /**
     * @return {Boolean}
     */
    function _checkCanvasPreMultipliedAlpha() {
        if (_canvasPreMultipliedAlpha === null) {
            var c = document.createElement("canvas");
            c.width = c.height = 1;
            var ctx = c.getContext("2d");
            var idata = ctx.getImageData(0, 0, 1, 1);
            idata.data[0] = 255;
            idata.data[3] = 1; // アルファを0にするとRGB値も自動的に0されてしまう場合がある
            ctx.putImageData(idata, 0, 0);
            idata = ctx.getImageData(0, 0, 1, 1);

            _canvasPreMultipliedAlpha = idata.data[0] !== 255;
        }
        
        return _canvasPreMultipliedAlpha;
    }
    
    /**
     * @param {Image} image
     * @return {Object}
     */
    function _createCanvas(image, w, h) {
        var w = w || image.width;
        var h = h || image.height;
        
        var canvas = document.createElement("canvas");
        canvas.width = w;
        canvas.height = h;
        var context = canvas.getContext("2d");
        context.scale(w / image.width, h / image.height);
        context.drawImage(image, 0, 0);
        
        return {
            canvas: canvas,
            context: context,
            imageData: context.getImageData(0, 0, w, h)
        };
    }
}(jQuery);
