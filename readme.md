# jQuery Plugin "MaskedImage"
## なんぞや？
JPEG画像をグレースケール画像でマスクするやつだよ。  
何が良いって？大きな画像をPNGより少ないデータで透過表示できるよ。  
通信量制限のあるモバイル通信にはもってこいだね。

![ゆっくりしていってね！](https://bytebucket.org/surface0/jquery.maskedimage/raw/79e2afbc053fdc376c0737d8a65a1e78bfddd9db/example.jpg)

SVGもしくはCanvasが使えるブラウザが必要だよ。  
古いブラウザはCanvasしか使えないよ。  
ちなみにAndroidの古いブラウザはCanvasで前乗算アルファを使うよ。  

## つかいかた
サンプルは付属のtestフォルダの中身を参照するといいよ。  
それにならってベース画像とグレースケールのマスク画像を対で用意してね。  

はじめにjQueryとこのプラグインを一緒に読み込ませる必要があるよ。  
jQueryはバージョン1.6以上が必須だよ。

```html
<script src="http://code.jquery.com/jquery-2.2.0.min.js"></script>
<script src="../src/jquery.MaskedImage.js"></script>
```

imgタグのsrc属性はベース画像URI、data-mask属性にマスク画像URIを入れてね。  

```html
<img src="base.jpg" data-mask="mask.jpg" width="290" height="500" alt="ゆっくりしていってね！">
```

img要素のjQueryオブジェクトに対してMaskedImageメソッドを実行するだけだよ。簡単だね。

```js
// 通常はこれでいいよ（SVGがだめだったら自動的にCanvas使うよ）
$("img").MaskedIamge();

// SVGを使わないようにするよ（ImageでdataスキームPNG使用）
$("img").MaskedImage({
	useSvg: false
});

// Imageではなく、Canvas使用と前乗算アルファを強制するよ
$("img").MaskedImage({
	useSvg: false,
	useImage: false,
	useMultipliedAlpha: true,
});

// ちょっと違う使い方もできるよ（非同期）
$.MaskedImage({
    url: [
        "images/base.jpg",
        "images/mask.jpg"
    ],
    useSvg: false,
}).done(function(result) {
    $("#img7").append(result);
});
```

## 注意してね
セキュリティの都合上、Canvasがローカルファイルやクロスオリジンではブロックされることがあるよ。  
ローカルファイルをGoogle Chromeで見る場合は、ショートカットの実行パスの末尾に以下のようなオプションをくっつけるといいよ。

```
"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --allow-file-access-from-files
```

## これだけ検証したよ
オートならみんなちゃんと映るよ。(PMA:前乗算アルファ）

| Browser                                      | SVG            | Canvas/Image  |
| :------------------------------------------- | :------------: | :-----: |
| Windows 7 Internet Explorer 11               | OK             | OK      |
| Windows 7 Chrome 48                          | OK             | OK      |
| Windows 7 Firefox 44                         | OK             | OK      |
| Windows 7 Opera 35                           | OK             | OK      |
| iPhone3GS iOS 6.1.5 Safari                   | OK             | OK      |
| iPhone6s Plus iOS9.0.1 Safari                | OK             | OK      |
| MEDIAS N04-C Android2.3.4 標準ブラウザ         | NG             | OK(PMA) |
| Xperia UL Android4.2.2 標準ブラウザ            | NG             | OK(PMA) |
| Xperia A4 Android5.0.2 標準ブラウザ            | OK             | OK      |
| Xperia A4 Android5.0.2 Chrome 48             | OK             | OK      |
| Galaxy S2 SC-02C Android4.0.3 標準ブラウザ     | NG             | OK(PMA) |
| Galaxy S2 SC-02C Android4.0.3 Chrome 48      | OK             | OK      |
| Galaxy S4 SC-04F Android4.4.2 標準ブラウザ     | OK             | OK      |
| Galaxy Nexus SC-04D Android4.2.2 標準ブラウザ  | NG             | OK(PMA) |
| INFOBAR A02 Android4.2.2 標準ブラウザ          | NG             | OK      |
| Freetel Preori2 FT142A Android5.0.2          | OK             | OK      |

※Android2.x系などCanvasのtoDataUrlメソッドが使えない場合はCanvas要素を使用するよ。

## 検証に使わせていただいた素材
* 黒と黄色のまんじゅう
	http://commons.nicovideo.jp/material/nc26482
	
## 免責事項
* 本プログラムを利用して生じた損害に対する責任を作者は一切負わないよ。
* 著作権に関してライセンスの設定は特に検討していないよ。コードの転載、改変は自由に行っていいよ。